'use strict';

// load files
require('../humans.txt');
//require('../manifest.json');
//require('../manifest.webapp');
require('../robots.txt');

// load images
require('../i/logo.png');
require('../i/favicon.png');
require('../i/touch/apple-touch-icon.png');
require('../i/touch/chrome-touch-icon-192x192.png');
require('../i/touch/icon-128x128.png');
require('../i/touch/ms-touch-icon-144x144-precomposed.png');

// load less
require('../less/app.less');

var $ = require('jquery');
var AMUI = require('amazeui');

$(function() {
  var fcvSideNav = $('#fcv-sidenav');
  if(fcvSideNav.length>0){
    // 首页锚点导航
    $(window).scroll(function(){
    var s = $(window).scrollTop();
      if(s > 300){
        $('#fcv-sidenav').show();
      }else if(s<200){
        $('#fcv-sidenav').hide();
      }
    });
  }

  // 首页轮播图
  var fcvSlider = $('.fcv-slider');
  if(fcvSlider.length>0){
    var fcvSliderLi = fcvSlider.find('li.fcv-news-block');
    var mySwiper = new Swiper ('.swiper-container', {
      autoplay : {
        delay: 2500,
        disableOnInteraction: false,
      },
      on : {
        slideChange: function(){
          var nextIndex = mySwiper.activeIndex;
          fcvSlider.find('li.active').removeClass('active');
          fcvSliderLi.eq(nextIndex).addClass('active');
        },
      },

      direction: 'vertical',
      onlyExternal : true
    });
    fcvSliderLi.hover(
      function () {
        mySwiper.autoplay.stop();
        fcvSlider.find('li.active').removeClass('active');
        $(this).addClass("active");
        var currIndex = fcvSliderLi.index(fcvSlider.find('li.active'));
        mySwiper.slideTo(currIndex,0.3,false);
      },
      function () {
        mySwiper.autoplay.start();
        //$(this).removeClass("active");
      }
    );
  }
  // end slider
  // 示范城市文字
  $('.fcv-citys-img a').hover(
    function () {
      $(this).find('.fcv-city-name').addClass('am-animation-scale-down');
    },
    function () {
      $(this).find('.fcv-city-name').removeClass("am-animation-scale-down");
    }
  );
  // 往期回顾齿轮动画
  $('.fcv-review-hexagon').hover(
    function () {
      $(this).find('img').addClass('am-animation-spin');
    },
    function () {
      $(this).find('img').removeClass("am-animation-spin");
    }
  );
  // 底部广告层
  $('.fcv-footer-adimg>.am-close').on('click',function(e){
    e.preventDefault();
    $('.fcv-footer-adimg').addClass('disabled');
  });
});
