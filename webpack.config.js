'use strict';

const path = require('path');
const glob = require('glob');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';
const WEBSITE_DOMAIN = isProduction ? 'http://fcv.wangshouxin.cn' : 'http://fcv.wangshouxin.cn';
const minPostfix = isProduction ? '.min' : '';
const minify = isProduction ? 'minimize' : '';
const hash = '[hash:7]';

const entry = './app/js/entry.js';
const devEntry = [
  'webpack/hot/dev-server',
  'webpack-hot-middleware/client?reload=true',
  entry,
];
const basePlugins = [
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    }
  }),
  new ExtractTextPlugin(`css/style.${hash}${minPostfix}.css`, {
    allChunks: true
  }),
];

const envPlugins = isProduction ? [
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false
    }
  }),
  new webpack.BannerPlugin(`build: ${new Date().toString()}`),
] : [
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.HotModuleReplacementPlugin(),
  // @see https://www.npmjs.com/package/eslint-loader#noerrorsplugin
  new webpack.NoErrorsPlugin(),
];

const config = {
  debug: !isProduction,
  devtool: !isProduction ? '#eval' : null,

  entry: isProduction ? entry : devEntry,

  output: {
    path: path.join(__dirname, 'dist'),
    filename: `js/app.${hash}${minPostfix}.js`,
    publicPath: isProduction ? '' : '/'
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: [
          // 'eslint',
        ],
        include: [
          path.join(__dirname, 'app/js')
        ]
      },
      {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract(
          'style',
          `css?${minify}!postcss!less`
        ),
        include: [
          path.join(__dirname, 'app/less')
        ]
      },
      {
        test: /\.jpe?g$|\.gif$|\.png|\.ico$/,
        loaders: [
          'file?name=[path][name].[ext]&context=app',
          'image-webpack'
          ]
      },
      {
        test: /\.txt$|\.json$|\.webapp$/,
        loader: 'file?name=[path][name].[ext]&context=app'
      },
    ]
  },

  plugins: basePlugins.concat(envPlugins),

  externals: {
    'jquery': 'jQuery',
    'amazeui': 'AMUI'
  },

  // watch: !isProduction,

  // loader config
  postcss: [autoprefixer({browsers: ['> 1%', 'last 2 versions']})],

  // @see https://www.npmjs.com/package/image-webpack-loader
  imageWebpackLoader: {}
};
// 多页面
const pages = getEntry('app/**.html','app/');
for(let filename in pages){
  const conf = {
    filename: filename.replace(new RegExp('app'), '')+'.html',
    template: pages[filename],
    inject: false,
    prod: isProduction,
    AMUICDN: isProduction ? 'https://cdnjs.cloudflare.com/ajax/libs/amazeui/2.7.2/' : '',
    minify: isProduction ? {
      removeComments: false,
      collapseWhitespace: false
    } : null,
    tplpath: isProduction ? '/html/dist/' : '',
    static: isProduction ? '/static/' : (WEBSITE_DOMAIN + '/static/'),
  }
  config.plugins.push(new HTMLWebpackPlugin(conf));
}
module.exports = config;

function getEntry(globPath, pathDir) {
  let files = glob.sync(globPath);
  let entries = {};
  let entry, dirname, basename, pathname, extname;
  for (let i = 0; i < files.length; i++) {
    entry = files[i];
    dirname = path.dirname(entry);
    extname = path.extname(entry);
    basename = path.basename(entry, extname);
    pathname = path.join(dirname, basename);
    pathname = pathDir ? pathname.replace(new RegExp('^' + pathDir), '') : pathname;
    entries[pathname] = './' + entry;
  }
  console.log(entries);
  return entries;
}
